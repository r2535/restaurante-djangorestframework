from django.db import models

# Create your models here.

class Client(models.Model):
    name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)
    observation = models.TextField(blank=True)

    def __str__(self):
        return self.name


class Waiter(models.Model):
    name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)

    def __str__(self):
        return self.name


class Table(models.Model):
    amount_diners = models.PositiveSmallIntegerField()
    location = models.CharField(max_length=45)

    def __str__(self):
        return self.location


class Product(models.Model):
    TYPES = (
        ('entrada', 'Entrada'),
        ('plato fuerte', 'Plato fuerte'),
        ('bebida', 'Bebida'),
        ('adicional', 'Adicional'),
    )
    name = models.CharField(max_length=150)
    price = models.FloatField()
    type = models.CharField(max_length=20, choices=TYPES, default=None)

    def __str__(self):
        return self.name


class Invoice(models.Model):
    date = models.DateTimeField()
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    waiter = models.ForeignKey(Waiter, on_delete=models.CASCADE)
    table = models.ForeignKey(Table, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.date} {self.waiter}'


class Order(models.Model):
    amount_products = models.IntegerField()
    products = models.ForeignKey(Product, on_delete=models.CASCADE)
    invoices = models.ForeignKey(Invoice, on_delete=models.CASCADE)




