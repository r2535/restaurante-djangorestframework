
from django.urls import reverse_lazy
from rest.forms import ClientForm, WaiterForm, TableForm, ProductForm, InvoiceForm, OrderForm
from rest.models import Client, Waiter, Table, Product, Invoice, Order
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView,
)
# Create your views here.

class ListClient(ListView):
    model = Client
    template_name = 'cliente/list_client.html'
    ordering = 'id'


class DetailClient(DetailView):
    model = Client
    template_name = 'cliente/detail_client.html'


class CreateClient(CreateView):
    model = Client
    template_name = 'cliente/create_client.html'
    form_class = ClientForm
    success_url = '/'

class UpdateClient(UpdateView):
    model = Client
    template_name = 'cliente/create_client.html'
    fields = ['name', 'last_name', 'observation']
    success_url = '/'


class DeleteClient(DeleteView):
    model = Client
    template_name = 'cliente/delete_client.html'
    success_url = reverse_lazy('list_client')


class ListWaiter(ListView):
    model = Waiter
    template_name = 'waiter/list_waiter.html'
    ordering = 'id'


class DetailWaiter(DetailView):
    model = Waiter
    template_name = 'waiter/detail_waiter.html'


class CreateWaiter(CreateView):
    model = Waiter
    template_name = 'waiter/create_waiter.html'
    form_class = WaiterForm
    success_url = reverse_lazy('list_waiter')


class UpdateWaiter(UpdateView):
    model = Waiter
    template_name = 'waiter/create_waiter.html'
    fields = ['name', 'last_name']
    success_url = reverse_lazy('list_waiter')


class DeleteWaiter(DeleteView):
    model = Waiter
    template_name = 'waiter/delete_waiter.html'
    success_url = reverse_lazy('list_waiter')


class ListTable(ListView):
    model = Table
    template_name = 'table/list_table.html'
    ordering = 'id'


class DetailTable(DetailView):
    model = Table
    template_name = 'table/detail_table.html'


class CreateTable(CreateView):
    model = Table
    template_name = 'table/create_table.html'
    form_class = TableForm
    success_url = reverse_lazy('list_table')


class UpdateTable(UpdateView):
    model = Table
    template_name = 'table/create_table.html'
    fields = ['amount_diners', 'location']
    success_url = reverse_lazy('list_table')


class DeleteTable(DeleteView):
    model = Table
    template_name = 'table/delete_table.html'
    success_url = reverse_lazy('list_table')


class ListProduct(ListView):
    model = Product
    template_name = 'product/list_product.html'
    ordering = 'id'


class DetailProduct(DetailView):
    model = Product
    template_name = 'product/detail_product.html'


class CreateProduct(CreateView):
    model = Product
    template_name = 'product/create_product.html'
    form_class = ProductForm
    success_url = reverse_lazy('list_product')


class UpdateProduct(UpdateView):
    model = Product
    template_name = 'product/create_product.html'
    fields = ['name', 'price', 'type']
    success_url = reverse_lazy('list_product')


class DeleteProduct(DeleteView):
    model = Product
    template_name = 'product/delete_product.html'
    success_url = reverse_lazy('list_product')


class ListInvoice(ListView):
    model = Invoice
    template_name = 'invoice/list_invoice.html'
    ordering = 'id'


class DetailInvoice(DetailView):
    model = Invoice
    template_name = 'invoice/detail_invoice.html'


class CreateInvoice(CreateView):
    model = Invoice
    template_name = 'invoice/create_invoice.html'
    form_class = OrderForm
    success_url = reverse_lazy('list_invoice')


class UpdateInvoice(UpdateView):
    model = Invoice
    template_name = 'invoice/create_invoice.html'
    fields = ['date', 'client', 'waiter', 'table']
    success_url = reverse_lazy('list_invoice')


class DeleteInvoice(DeleteView):
    model = Invoice
    template_name = 'invoice/delete_invoice.html'
    success_url = reverse_lazy('list_invoice')


class ListOrder(ListView):
    model = Order
    template_name = 'order/list_order.html'
    ordering = 'id'


class DetailOrder(DetailView):
    model = Order
    template_name = 'order/detail_order.html'


class CreateOrder(CreateView):
    model = Order
    template_name = 'order/create_order.html'
    form_class = OrderForm
    success_url = reverse_lazy('list_order')


class UpdateOrder(UpdateView):
    model = Order
    template_name = 'order/create_order.html'
    fields = ['amount_products', 'products', 'invoices']
    success_url = reverse_lazy('list_order')


class DeleteOrder(DeleteView):
    model = Order
    template_name = 'order/delete_order.html'
    success_url = reverse_lazy('list_order')
