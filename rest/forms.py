from django import forms
from .models import Client, Waiter, Table, Product, Invoice, Order


class ClientForm(forms.ModelForm):
    class Meta:
        model = Client

        fields = [
            "name",
            "last_name",
            "observation",
        ]


class WaiterForm(forms.ModelForm):
    class Meta:
        model = Waiter
        fields = [
            "name",
            "last_name",
        ]


class TableForm(forms.ModelForm):
    class Meta:
        model = Table
        fields = [
            "amount_diners",
            "location",
        ]


class ProductForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = [
            "name",
            "price",
            "type",
        ]


class InvoiceForm(forms.ModelForm):
    date = forms.DateTimeField(widget=forms.SelectDateWidget)
    client = forms.ModelChoiceField(queryset=Client.objects.all())
    waiter = forms.ModelChoiceField(queryset=Waiter.objects.all())
    table = forms.ModelChoiceField(queryset=Table.objects.all())

    class Meta:
        model = Invoice
        fields = [
            "date",
            "client",
            "waiter",
            "table",
        ]


class OrderForm(forms.ModelForm):
    amount_products = forms.IntegerField()
    products = forms.ModelChoiceField(queryset=Product.objects.all())
    invoices = forms.ModelChoiceField(queryset=Invoice.objects.all())

    class Meta:
        model = Order
        fields = [
            "amount_products",
            "products",
            "invoices",
        ]