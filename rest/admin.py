from django.contrib import admin
from rest.models import Client, Waiter, Product, Table, Order, Invoice


# Register your models here.

@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'last_name',
        'observation',
    )

@admin.register(Waiter)
class WaiterAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'last_name',
    )

@admin.register(Table)
class TableAdmin(admin.ModelAdmin):
    list_display = (
        'amount_diners',
        'location',
    )

@admin.register(Product)
class ProductsAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'price',
        'type',
    )

@admin.register(Invoice)
class InvoiceAdmin(admin.ModelAdmin):
    list_display = (
        'date',
        'client',
        'waiter',
        'table',
    )


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = (
        'amount_products',
        'products',
        'invoices',
    )
