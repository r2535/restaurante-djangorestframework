from django.contrib.auth.models import User
from rest_framework import viewsets

from rest.api.serializer import (
                                    ClientSerializer,
                                    WaiterSerializer,
                                    TableSerializer,
                                    ProductSerializer,
                                    InvoiceSerializer,
                                    OrderSerializer,
)
from rest.models import Client, Waiter, Table, Product, Invoice, Order


class ClientViewSet(viewsets.ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class WaiterViewSet(viewsets.ModelViewSet):
    queryset = Waiter.objects.all()
    serializer_class = WaiterSerializer


class TableViewSet(viewsets.ModelViewSet):
    queryset = Table.objects.all()
    serializer_class = TableSerializer


class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer


class InvoiceViewSet(viewsets.ModelViewSet):
    queryset = Invoice.objects.all()
    serializer_class = InvoiceSerializer


class OrderViewSet(viewsets.ModelViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
