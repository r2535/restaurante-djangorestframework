from django.contrib.auth.models import User
from rest_framework import serializers
from rest.models import Waiter, Client, Table, Product, Invoice, Order


#nos muestra la serializacion atravez de un link, si usamos esta forma debemos colocar
# el campo 'url', dentro de los fields


class ClientSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Client
        fields = ['url', 'name', 'last_name', 'observation']


# otra forma de serializar

class WaiterSerializer(serializers.ModelSerializer):
    class Meta:
        model = Waiter
        fields = ['id', 'name', 'last_name']


class TableSerializer(serializers.ModelSerializer):
    class Meta:
        model = Table
        fields = '__all__'


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = '__all__'


class InvoiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Invoice
        fields = '__all__'


class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = '__all__'


