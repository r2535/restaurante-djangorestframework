from django.urls import path
from rest_framework import routers



from rest.api.views import ClientViewSet, WaiterViewSet, TableViewSet, ProductViewSet, InvoiceViewSet, OrderViewSet

router = routers.DefaultRouter()
router.register(r'client', ClientViewSet)
router.register(r'waiter', WaiterViewSet)
router.register(r'table', TableViewSet)
router.register(r'product', ProductViewSet)
router.register(r'invoice', InvoiceViewSet)
router.register(r'order', OrderViewSet)
