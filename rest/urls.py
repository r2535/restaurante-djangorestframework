from django.urls import path
from rest.views import (
    ListClient, DetailClient, CreateClient, UpdateClient, DeleteClient,
    ListWaiter, DetailWaiter, CreateWaiter, UpdateWaiter, DeleteWaiter,
    ListTable,  DetailTable,  CreateTable, UpdateTable, DeleteTable,
    ListProduct, DetailProduct, CreateProduct, UpdateProduct, DeleteProduct,
    CreateInvoice, ListInvoice, DetailInvoice, UpdateInvoice, DeleteInvoice,
    ListOrder, DetailOrder, CreateOrder, UpdateOrder, DeleteOrder,
)

urlpatterns = [
    path('', ListClient.as_view(), name='list_client'),
    path('detail_client/', DetailClient.as_view(), name='detail_client'),
    path('create_client/', CreateClient.as_view(), name='create_client'),
    path('update_client/<int:pk>/', UpdateClient.as_view(), name='update_client'),
    path('delete_client/<int:pk>/', DeleteClient.as_view(), name='delete_client'),

    path('list_waiter/', ListWaiter.as_view(), name='list_waiter'),
    path('detail_waiter/', DetailWaiter.as_view(), name='detail_waiter'),
    path('create_waiter/', CreateWaiter.as_view(), name='create_waiter'),
    path('update_waiter/<int:pk>/', UpdateWaiter.as_view(), name='update_waiter'),
    path('delete_waiter/<int:pk>/', DeleteWaiter.as_view(), name='delete_waiter'),

    path('list_table/', ListTable.as_view(), name='list_table'),
    path('detail_table/', DetailTable.as_view(), name='detail_table'),
    path('create_table/', CreateTable.as_view(), name='create_table'),
    path('update_table/<int:pk>/', UpdateTable.as_view(), name='update_table'),
    path('delete_table/<int:pk>/', DeleteTable.as_view(), name='delete_table'),

    path('list_product/', ListProduct.as_view(), name='list_product'),
    path('detail_product/', DetailProduct.as_view(), name='detail_product'),
    path('create_product/', CreateProduct.as_view(), name='create_product'),
    path('update_product/<int:pk>/', UpdateProduct.as_view(), name='update_product'),
    path('delete_product/<int:pk>/', DeleteProduct.as_view(), name='delete_product'),

    path('list_invoice/', ListInvoice.as_view(), name='list_invoice'),
    path('detail_invoice/', DetailInvoice.as_view(), name='detail_invoice'),
    path('create_invoice/', CreateInvoice.as_view(), name='create_invoice'),
    path('update_invoice/<int:pk>/', UpdateInvoice.as_view(), name='update_invoice'),
    path('delete_invoice/<int:pk>/', DeleteInvoice.as_view(), name='delete_invoice'),

    path('list_order/', ListOrder.as_view(), name='list_order'),
    path('detail_order/', DetailOrder.as_view(), name='detail_order'),
    path('create_order/', CreateOrder.as_view(), name='create_order'),
    path('update_order/<int:pk>/', UpdateOrder.as_view(), name='update_order'),
    path('delete_order/<int:pk>/', DeleteOrder.as_view(), name='delete_order'),



]